/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TouchableOpacity,Button,Image ,Dimensions} from 'react-native';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import MasterX from "./MasterX";
const {width,height} = Dimensions.get('window');
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <MasterX
          content={<HeaderImageScrollView
                maxHeight={250}
                minHeight={70}
                renderHeader={() => (
                    <Image source={require('./a.jpg')} resizeMode="cover" style={{height:200,width:width}} />
                )}
                renderForeground={() => (
                    <View>
                     <Text style={{textAlign:'center',color:'white'}}>Osman Aşar</Text>
                    </View>
                 )}
              >
                <View style={{ height: 1000 }}>
                  <TriggeringView onHide={() => console.log('text hidden')} >
                    <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
                    <TouchableOpacity onPress={()=>dropdown.alertWithType("success","error","Osman")}>
                    <Text>Osman</Text>
                    </TouchableOpacity>
                    </TriggeringView>

                </View>
                <View >

                    <Text>Scroll Me!</Text>
                    <TouchableOpacity onPress={()=>dropdown.alertWithType("error","error","Osman")}>
                    <Text>Osman</Text>
                    </TouchableOpacity>


                </View>
              </HeaderImageScrollView>
            }/>



    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
