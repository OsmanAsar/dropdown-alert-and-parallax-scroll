import React ,{Component} from "react";
import {View,Text} from "react-native";
import {Router,Stack,Scene} from "react-native-router-flux";
import DropdownAlert from 'react-native-dropdownalert';
import App from './App';


class MasterX extends Component{
  render(){
    return(
      <View style={{flex:1}}>
        {this.props.content}
        <DropdownAlert ref={ref => dropdown = ref} />
      </View>
    );
  }
}
export default MasterX;
