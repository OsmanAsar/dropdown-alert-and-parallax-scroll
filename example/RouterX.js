import React ,{Component} from "react";
import {View} from "react-native";
import {Router,Stack,Scene} from "react-native-router-flux";
import DropdownAlert from 'react-native-dropdownalert';
import App from './App';


class RouterX extends Component{
  render(){
    return(
      <Router>

      <Stack key="root">
        <Scene key="app" component={App} title="App" hideNavBar={true} />

      </Stack>

      </Router>
);
  }
}
export default RouterX;
