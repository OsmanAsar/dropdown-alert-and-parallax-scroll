/** @format */

import {AppRegistry} from 'react-native';
import RouterX from './RouterX';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => RouterX);
